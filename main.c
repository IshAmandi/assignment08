// C Program to Store Information
// of Students Using Structure

#include <stdio.h>
#include <string.h>

// Create the student structure
struct Student
{
        char first_name[20];
        char subject;
        double marks;
};


int main()
{
        int noStudents;
        printf("Enter no of Students greater than or equal 5 :\n");
        scanf("%d", &noStudents);

     // Create the student's structure variable
	// with n Student's records
	struct Student student[noStudents];

	// Get the students data
        for (int i= 0; i<noStudents; i++)
        {
                printf("Enter the Student Records :%d\n" , i+1);
                printf(" Enter First_Name :\n");
                scanf("%s", &student[i].first_name);
                printf(" Enter the Subject :\n");
                scanf("%s", &student[i].subject);
                printf(" Enter the Marks :\n\n");
                scanf("%f", &student[i].marks);
        }


	// Display the Students information
        for (int i = 0; i < noStudents; i++)

        {
                printf("\n\nDisplay Student Records:\n\n" , i+1);
                printf("\nFirst_Name : %s", student[i].first_name);
                printf("\nSubject : %s", student[i].subject);
                printf("\nMarks : %0.2f\n\n", student[i].marks);

        }

        return 0;
}
